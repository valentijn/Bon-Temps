<?php

class Guest
{
    // Add more classes by in the form "database_name" => "classname"
    public $perms = array("Gebruiker" => "User", "Adminstator" => "Adminstrator");

    function __construct($webshop) {
        $this->webshop = $webshop;
        $this->logger = new Logger("users");
        $this->winkelwagen = [];
        $this->logged_in = false;
    }

    function __wakeup() {
        $this->webshop = unserialize($this->webshop);
    }

    function __sleep() {
        $this->webshop = serialize($this->webshop);
        return array_keys(get_object_vars($this));
    }

    function get_webshop() {
        return $this->webshop;
    }

    function get_winkelwagen() {
        return $this->winkelwagen;
    }

    function get_logged_in() {
        return $this->logged_in;
    }

    function add_to_winkelwagen($item) {
        $this->logger->info("Adding " . $item + " to winkelwagen");
        array_push($this->winkelwagen, $item);
    }

    function remove_from_winkelwagen($item) {
        $this->logger->info("Removing " . $item + " from winkelwagen");
        $this->winkelwagen = array_merge(array_diff($this->winkelwagen, array($item)));
    }

    function register($username, $password, $email) {
        $hash = password_hash($password, PASSWORD_DEFAULT);
        return $this->webshop->add_user($username, $hash, $email, "Gebruiker");
    }

    function login($username, $password) {
        $this->logger->info($username . " logging in");
        $hash = password_hash($password, PASSWORD_DEFAULT);
        $sql = "SELECT pi_code, password, email,permissies FROM gebruikers WHERE ";
        $sql .= "account_naam=:naam;";
        $prep = $this->webshop->db->prepare($sql);
        $prep->bindValue(':naam', $username, SQLITE3_TEXT);

        $result = $prep->execute()->fetchArray(SQLITE3_ASSOC);

        if (password_verify($password, $result['password'])) {
            $class = $this->perms[$result['permissies']];
            return new $class($this->webshop,
                              $this->winkelwagen,
                              $result['pi_code'],
                              $username,
                              $result['email']);
        } else {
            return false;
        }
    }
}

class User extends Guest
{
    function __construct($webshop, $winkelwagen, $pi, $username, $email) {
        parent::__construct($webshop, $winkelwagen);
        $this->logged_in = true;
        $this->pi = $pi;
        $this->username = $username;
        $this->email = $email;
    }

    function get_username() {
        return $this->username;
    }

    function get_pi() {
        return $this->pi;
    }

    function logout() {
        return new Guest($this->webshop, $this->winkelwagen);
    }

    function reset_password($password) {
        $this->logger->info("Resetting password for " . $this->username);
        $hash = password_hash($password, PASSWORD_DEFAULT);
        $sql = "UPDATE gebruikers SET password=:pass WHERE account_naam=:username;";
        $prep = $this->webshop->db->prepare($sql);

        $prep->bindValue(":pass", $hash, SQLITE3_TEXT);
        $prep->bindValue(":username", $this->username, SQLITE3_TEXT);

        return $prep->execute();
    }


    function remove() {

        $sql = "DELETE FROM gebruikers WHERE account_naam='" . $this->username . "';";
        $result = $this->webshop->exec($sql);

        if (! $result)
            $this->logger->warning("Removing user " . $this->username);
        else
            $this->logger->error("Failed to remove user " . $this->username);

        return $result;
    }
}

class Adminstrator extends User
{

}

?>
