<?php
require_once("init.php");
require_once("utils.php");

function errors($errno, $errstr) {
    unserialize($_SESSION['user'])->logger->error(sprintf("%s (%s)", errno, errstr));
}

function remove() {
    set_error_handler(function () {});
    $result = unserialize(unserialize($_SESSION['user'])->remove());
    restore_error_handler();

    if ($result) {
        exit_code(1, "removing failed");
    } else {
        exit_code(0, "Removing succeeded");
    }
}

function login($username, $password)
{
    set_error_handler(function () {});
    $result = unserialize($_SESSION['user'])->login($username, $password);
    restore_error_handler();

    if (! $result) {
        exit_code(1, "Login failed");
    } else {
        $_SESSION['user'] = serialize($result);
        exit_code(0, "Login succeeded ");
    }
}

function logout()
{
    $user = unserialize($_SESSION['user']);
    checkUser(get_class($user), "User");
    $_SESSION['user'] = serialize($user->logout());
}

function password_reset($password) {
    $user = unserialize($_SESSION['user']);
    checkUser(get_class($user), "User");
    //set_error_handler(function () {});
    $result = $user->reset_password($password);
    restore_error_handler();

    if (! $result) {
        exit_code(3, "Password reset failed");
    } else {
        exit_code(0, "Password reset succeeded");
    }
}

function register($username, $password, $email)
{
    set_error_handler(function () {});
    $result = unserialize($_SESSION['user'])->register($username, $password, $email);
    restore_error_handler();

    if (! $result)
        exit_code(2, "register failed");
    else
        login($username, $password);
}

if (isset($_POST['login'])) {
    login($_POST['username'], $_POST['password']);
} else if (isset($_POST['register'])) {
    register($_POST['username'], $_POST['password'], $_POST['email']);
} else if (isset($_POST['remove'])) {
    remove();
} else if (isset($_POST['logout'])) {
    logout();
} else if (isset($_POST['password_reset'])) {
    password_reset($_POST['password']);
}

?>
