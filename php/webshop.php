<?php
include("logging.php");

class Webshop {
    public $db;

    function __construct($name) {
        $this->connect_database($name);
        $this->logger = new Logger("Webshop");
        $this->db_name = $name;
        $this->logger->info("Connected to " . $name);
    }

    function __wakeup() {
        $this->connect_database($this->db_name);
    }

    function connect_database($name) {
        $db = new SQLite3($name, SQLITE3_OPEN_READWRITE);

        if (! $db)
            echo $db->lastErrorMsg();

        $this->db = $db;
    }

    function get_db() {
        return $this->db;
    }

    function add_user($name, $password, $email, $permissies) {
        $this->logger->info("Adding user: " . $name);

        $sql = "SELECT max(pi_code) FROM persoonlijke_informatie;";
        $r="INSERT INTO gebruikers VALUES(:account_naam, :password, :pi_code, :permissies, :email)";
        $pi_code = $this->execSingle($sql)['max(pi_code)'];
        $reg_s = $this->db->prepare($r);

        $reg_s->bindValue(":account_naam", $name, SQLITE3_TEXT);
        $reg_s->bindValue(":password", $password, SQLITE3_TEXT);
        $reg_s->bindValue(":pi_code", $pi_code, SQLITE3_INTEGER);
        $reg_s->bindValue(":permissies", $permissies, SQLITE3_TEXT);
        $reg_s->bindValue(":email", $email, SQLITE3_TEXT);

        return $reg_s->execute();
    }

    function exec($query) {
        $this->logger->info("Executing: " . $query);
        $this->db->exec($query);
    }

    function execSingle($query) {
        $this->logger->info("Executing: " . $query);
        return $this->db->querySingle($query, true);
    }

    function exec_n_fetch($query) {
        $this->logger->info("Executing: " . $query);
        $result = $this->db->query($query);
        $answer = array($result->fetchArray(SQLITE3_ASSOC));

        while ($array = $result->fetchArray(SQLITE3_ASSOC))
            array_push($answer, $array);

        return $answer;
    }
}
?>
