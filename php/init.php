<?php
function getPath($path)
{
    return realpath(dirname(__FILE__) . '/' . $path);
}

// Start the session
session_start();

// Setup the template engine
require_once("/usr/share/php/Twig/Autoloader.php");
Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem(getPath("../static/templates"));
$twig = new Twig_Environment($loader, array('debug' => true, 'auto_reload' => true));
//if (!isset($_SESSION['twig']))
//$_SESSION['twig'] = serialize(new Twig_Environment($loader, array('debug' => true,
//'auto_reload' => true)));

// Setup the basics of the webshop
include_once("webshop.php");
include_once("user.php");

// Initialize the guest and webshop to ensure that webpagse always have valid input
if (!isset($_SESSION['user']))
    $_SESSION['webshop'] = serialize(new Webshop(getPath("../shop.db")));

if (!isset($_SESSION['user']))
    $_SESSION['user'] = serialize(new Guest(unserialize($_SESSION['webshop'])));
?>
