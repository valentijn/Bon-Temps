<?php

function printJSON($info)
{
    echo(json_encode($info));
    exit();
}

function addJSON($response, $name, $text)
{
    $GLOBALS['response'][$name] = $text;
    //    array_push($response, $text);
}

function exit_code($id, $text)
{
    echo(json_encode(array("error_code" => $id,
                           "response" => $text)));
}

function checkUser($user, $required)
{
    function getValue($class) {
        // Different types of users all of which with a value.
        // This emulates an enum in normal languages.
        $types = array("Guest" => 0, "User" => 1, "Adminstrator" => 2);

        return $types[$class];
    }

    if (getValue($user) < getValue($required)) {
        echo(exit_code(1, "Login failed"));
        exit();
    }
}

?>
