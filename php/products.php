<?php
require_once("init.php");

$webshop = unserialize($_SESSION['webshop']);

function showCat($db, $doelgroep, $lim){
  $sql = "SELECT * FROM categorie WHERE doelgroep = '$doelgroep'";
  $exec = $db->exec_n_fetch($sql);
  foreach ($exec as $categorie)
    echo("<a onclick='getProducts('".$categorie['categorie']."', '".$categorie['doelgroep']."', '".$lim."' )'>".$categorie['categorie']."><option>".$categorie['categorie']."</option></a>");
  }
function showCategorie($db)
  {
      $sql  = "SELECT * FROM categorie where doelgroep = \"vrouwen\"";
      $result = $db->exec_n_fetch($sql);

      foreach ($result as $option)
      {
          echo("<option value=\"".$option['categorienr']."\">".$option['categorie']."</option>");
          echo($option['categorie']);
      }
  }
function showMerk($db)
  {
      $sql  = "SELECT * FROM merk ";
      $result = $db->exec_n_fetch($sql);

      $counter = 0;

      foreach ($result as $option)
      {
          echo("<br><input name=\"".$option['merkID']."\" class=\"form-check-input\" id=\"merk\" type=\"checkbox\">".$option['merkNaam']);
      }
  }
function minMax($db) {
  $sql = "SELECT MIN(prijst), MAX(prijst) FROM items";
  $exec = $db->exec_n_fetch($sql);

  foreach ($exec as $value) {
    echo("<label for=\"formGroupExampleInput\">Minimaal</label><input class=\"form-control\" id=\"minP\" placeholder=\"Minimale Prijs\" value=".$value['MIN(prijst)']." >");
    echo("<label for=\"formGroupExampleInput\">Maximaal</label><input class=\"form-control\" id=\"maxP\" placeholder=\"Maximale Prijs\" value=".$value['MAX(prijst)']." >");

  }
}

if (isset($_GET['productAjax'])) {
  $merkid = json_decode($_GET['merkid']);
  $fill = join(',', array_fill(0, count($merkid), '?'));

  $sql = "SELECT * FROM `items` INNER JOIN `fotos` ON fotos.id = items.id INNER JOIN `categorie` ON categorie.categorienr = items.categorienr ";
  $sql .= "WHERE merkid IN ($fill) AND items.categorienr = :categorienr AND prijst >= :minimalePrijs AND prijst <= :maximalePrijs LIMIT :limit ";
  $reg_s = $webshop->db->prepare($sql);


  for ($i = 0; $i < count($merkid); $i++) {
    $reg_s->bindValue($i+1, $merkid[$i]);
  }

  $reg_s->bindvalue(":categorienr",$_GET['categorienr'],SQLITE3_TEXT);
  $reg_s->bindvalue(":minimalePrijs",$_GET['min'],SQLITE3_TEXT);
  $reg_s->bindvalue(":maximalePrijs",$_GET['max'],SQLITE3_TEXT);
  $reg_s->bindvalue(":limit",$_GET['lim'],SQLITE3_INTEGER);

  $return = $reg_s->execute();
  $result = array($return->fetchArray(SQLITE3_ASSOC));

  while ($array = $return->fetchArray(SQLITE3_ASSOC)) {
    array_push($result, $array);
  }
  echo(json_encode($result));
  exit();
}

//WHERE `categorie` = ".$_GET['categorie']." AND `doelgroep` = ".$_GET['doelgroep']."
?>
