<?php

class Logger {

    function __construct($name, $dir = "../logs", $timeformat = '%m:%H') {
        $this->filename = realpath(dirname(__FILE__) . '/' . $dir) . "/webshop.log";
        $this->name = $name;
        $this->log_file = fopen($this->filename, "a");
        $this->timeformat = $timeformat;
    }

    function __wakeup() {
        $this->log_file = fopen($this->filename, "a");
    }

    private function write2log($level, $message) {
        $time = strftime($this->timeformat);
        fwrite($this->log_file, sprintf("%s - [%s] - %s - %s\n", $time, $level, $this->name,
                                        $message));
    }

    function info($message) {
        $this->write2log("info", $message);
    }

    function warning($message) {
        $this->write2log("warning", $message);
    }

    function error($message) {
        $this->write2log("error", $message);
    }

    function parseline() {
        $ar = array();

        foreach (file($this->filename, FILE_IGNORE_NEW_LINES) as $line) {
            $line = explode("-", $line);
            array_push($ar, array("time" => $line[0],
                                  "type" => $line[1],
                                  "file" => $line[2],
                                  "message" => $line[3]));
        }

        return $ar;
    }
}

//$logger = new Logger("Fuck");
//$logger->info("foo");
//$logger->warning("test");
