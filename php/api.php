<?php
require_once("init.php");
require_once("utils.php");

$webshop = unserialize($_SESSION['webshop']);
$user = unserialize($_SESSION['user']);
$response = array();

function getFromDatabase($table, $row) {
    $sql = "SELECT " . $row . " FROM " . $table . ";";
    checkUser($GLOBALS['user'], "User");
    printJSON($GLOBALS['webshop']->exec_n_fetch($sql));
}

function getUserClass() {
    addJSON($GLOBALS['response'], "class", get_class($GLOBALS['user']));
}

function getWinkelwagen() {
    addJSON($GLOBALS['response'], "winkelwagen", $GLOBALS['user']->get_winkelwagen());
}

function getUsername() {
    addJSON($GLOBALS['response'], "username", $GLOBALS['user']->get_username());
}

foreach (array_keys($_GET) as $item) {
    switch ($item) {
        case "getFromDatabase":
            getFromDatabase($_GET['table'], $_GET['row']);
            break;

        case "getUserClass":
            getUserClass();
            break;

        case "getWinkelwagen":
            getWinkelwagen();
            break;

        case "getUsername":
            getUsername();
            break;
    }
}

printJSON($response);
?>
