<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Prociono" rel="stylesheet">
    <link href="static/index.css    " rel="stylesheet">
    <link href="static/style.css"    rel="stylesheet">
    <link href="static/fonts.css"    rel="stylesheet">
    <link href="static/stolen.css"   rel="stylesheet">
    <link href="static/products.css" rel="stylesheet">
  </head>
  <body>
    <!-- TOP NAVBAR -->
    <!-- TOP NAVBAR -->
    <!-- TOP NAVBAR -->
    <nav class="navbar navbar-inverse navbar-fixed-top" id="topbar">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
            data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Home</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div> <!-- Navbar collapse -->
      </div>
    </nav>
    <!-- NORMAL NAVBAR -->
    <!-- NORMAL NAVBAR -->
    <!-- NORMAL NAVBAR -->
    <nav class="navbar">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
            data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Home</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div> <!-- Navbar collapse -->
      </div>
    </nav>
    <div class="container-fluid" >
    <!-- CODE -->
        <!-- CODE -->
            <!-- CODE -->
        <?php
        include_once('php/webshop.php');
        include_once('php/products.php');
        $db = new Webshop("shop.db");
        $lim = 30;
        ?>
            <form class="form-horizontal col-xs-12 col-md-3 filter">
              <fieldset>

              <!-- Form Name -->
              <legend>Dames Filter</legend>
              <div class="form-group">
                <label for="formGroupExampleInput">Categorie</label>
                  <select class="form-control" id="categorieNR" name="categorie">
                      <option value="_"></option>
                      <?php
                      showCategorie($db);
                      ?>
                  </select>
              </div>

              <!-- Text input-->
              <div class="form-group">
                      <?php
                      minmax($db);
                       ?>
              </div>

              <!-- Merk Checkboxes -->
              <div class="form-group">
                <label for="formGroupExampleInput">Merken</label>
                      <?php
                      showMerk($db);
                      ?>
              </div>

              <!-- Button -->
              <div class="control-group">
                <div class="controls">
                  <input type="button" href="javascript:void(0)" onclick="getProducts()" name="singlebutton" value="filter" class="form-control form-control-lg btn btn-primary"></input>
                </div>
              </div>

              </fieldset>
              </form>


              </div>
            <div id="filter" class="col-xs-12 col=md-9 col-xl-9 productP row">

            </div>
    </div>










<!--FOOTER -->
    <!--FOOTER -->
        <!--FOOTER -->
        <footer class="footer-distributed footer">

            <div class="footer-left">

                <h3>Fi<span>fth</span></h3>

                <p class="footer-links">
                    <a href="index.php">Home</a>
                    ·
                    <a href="#">Blog</a>
                    ·
                    <a href="products.php">Products</a>
                    ·
                    <a href="aboutus.php">About</a>
                    ·
                    <a href="faq.php">Faq</a>
                    ·
                    <a href="contact.php">Contact</a>
                </p>

                <p class="footer-company-name"> ACP Webdesign &copy; 2017</p>
            </div>

            <div class="footer-center">

                <div>
                    <i class="fa fa-map-marker"></i>
                    <p><span>21 Revolution Street</span> Paris, France</p>
                </div>

                <div>
                    <i class="fa fa-phone"></i>
                    <p>+1 555 123456</p>
                </div>

                <div>
                    <i class="fa fa-envelope"></i>
                    <p><a href="mailto:support@fifth.com">support@fifth.com</a></p>
                </div>

            </div>

            <div class="footer-right">

                <p class="footer-company-about">
                    <span>About Us</span>
                    Wij zijn een hip jong bedrijf die gevestigd is in haarlem, wij richten ons op het jonge koppel
                </p>

                <div class="footer-icons">

                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-linkedin"></i></a>
                    <a href="#"><i class="fa fa-github"></i></a>

                </div>

            </div>

        </footer>


  </body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script>
  function getProducts() {
    function create_dropdown() {
        var categorienr = getElementById(#categorieNR).textContent;
        var limit = 30;
        var minP = getElementById(#minP).textContent;
        var maxP = getElementById(#maxP).textContent;
        var merk= $("input:checked");
        var merkid = [];
        for(var i = 0; i < merk.length; i++){
          merkid.push(merk[i].name);
        }
        merkid = JSON.stringify(merkid);

        $.getJSON("php/products.php", { productAjax: 1,categorienr: catnr, lim: limit, min: minP, max: maxP, merkid: merkid },
                  function (json) {
                      for (var i = 0; i < json.length; i++) {

                      }
                  }
        );
    }
  }
  </script>
</html>
