<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Prociono" rel="stylesheet">
    <link href="static/index.css" rel="stylesheet">
    <link href="static/style.css" rel="stylesheet">
    <link href="static/fonts.css" rel="stylesheet">
    <link href="static/stolen.css" rel="stylesheet">
  </head>
  <body>
    <!-- TOP NAVBAR -->
    <!-- TOP NAVBAR -->
    <!-- TOP NAVBAR -->
    <nav class="navbar navbar-inverse navbar-fixed-top" id="topbar">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
            data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Home</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div> <!-- Navbar collapse -->
      </div>
    </nav>
    <!-- NORMAL NAVBAR -->
    <!-- NORMAL NAVBAR -->
    <!-- normal navbar -->
    <nav class="navbar">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
            data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Home</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div> <!-- Navbar collapse -->
      </div>
    </nav>
    <div class="blog_container">
      <!-- blog Code-->
      <!-- blog Code-->
      <!-- blog Code-->
      <?php
      include_once('php/webshop.php');
      include_once('php/blogs.php');
      $db = new Webshop("shop.db");

      $id = 8;
      ?>
    </div>
    <div class="blog1">
        <div class='blog' usemap='blog'><div class='cold-md-12 blog'>
        <img id='bigImage' src="">
        </span></div></a></div></div>
    </div>
    <div class="container">
    <div class="col-md-7 col-xs-10 beschrijving">
        <p id='blogTitel' class="font-blogtitle"></p>
        <p id='blogTekst' class="font-blogtekst"></p>
    </div>
    <br>
    <div class="row relBlog">
    <h2>Andere relevante blogposts</h2>
    <hr class="thick">
        <a href='javascript:void(0)' id=" . $blog['id'] . " onclick='updateBlog(this.id)'><div class='relblog col-xs-6 col-md-3' usemap='blog'><div class='cold-md-12 blog'>
        <img src='" . $blog['foto'] . "'>
        </div class='titlebar' id='gradient><span class='font-blogtekst>
        </span></div></a>
    </div>
</div>
        <footer class="footer-distributed footer">

            <div class="footer-left">

                <h3>Fi<span>fth</span></h3>

                <p class="footer-links">
                    <a href="index.php">Home</a>
                    ·
                    <a href="#">Blog</a>
                    ·
                    <a href="products.php">Products</a>
                    ·
                    <a href="aboutus.php">About</a>
                    ·
                    <a href="faq.php">Faq</a>
                    ·
                    <a href="contact.php">Contact</a>
                </p>

                <p class="footer-company-name"> ACP Webdesign &copy; 2017</p>
            </div>

            <div class="footer-center">

                <div>
                    <i class="fa fa-map-marker"></i>
                    <p><span>21 Revolution Street</span> Paris, France</p>
                </div>

                <div>
                    <i class="fa fa-phone"></i>
                    <p>+1 555 123456</p>
                </div>

                <div>
                    <i class="fa fa-envelope"></i>
                    <p><a href="mailto:support@fifth.com">support@fifth.com</a></p>
                </div>

            </div>

            <div class="footer-right">

                <p class="footer-company-about">
                    <span>About Us</span>
                    Wij zijn een hip jong bedrijf die gevestigd is in haarlem, wij richten ons op het jonge koppel
                </p>

                <div class="footer-icons">

                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-linkedin"></i></a>
                    <a href="#"><i class="fa fa-github"></i></a>

                </div>

            </div>

        </footer>
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script>
    $.getJSON("php/blogs.php", { first: 1 }, function (json) { updateBlog(json[0]['MIN(id)'] )}  );

    function clickHandler(blog_id) {
        console.log(blog_id);
        updateBlog(blog_id);
    }
    function updateBlog(id) {
        $.getJSON("php/blogs.php", { ajax: 1 },
            function (json) {
                var current = 2;

                for (var i = 0; i < json.length; i++)
                    //console.log(json[i]['id'] === id) ;
                    if (json[i]['id'] == id)
                        current = json[i];

                document.getElementById("bigImage").src = current['foto']
                document.getElementById("blogTitel").textContent = current['titel']
                document.getElementById("blogTekst").textContent = current['tekst']
                console.log(current);
            }
        );
    //    var new_blog = document.createElement("p");
    //    document.getElementById("bigImage").src = ;
      //  var current_length = (photos.childNodes.length - 4 + 1) % 2;
    //    var base_name = "  foto " + ++counter + " : ";

    //    new_photo.innerHTML = base_name + "<input type='text' name='" + counter + "'>";
    //    photos.appendChild(new_photo);
    }
    </script>
</html>
