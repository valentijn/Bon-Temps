BEGIN TRANSACTION;
CREATE TABLE verkocht(
id INT REFERENCES items ON UPDATE restrict ON DELETE cascade,
aantal INT NOT NULL,
account_naam TEXT REFERENCES gebruikers ON UPDATE restrict ON DELETE cascade
);
CREATE TABLE persoonlijke_informatie
(
pi_code INT PRIMARY KEY,
adres TEXT NOT NULL,
postcode TEXT NOT NULL,
nummer INT NOT NULL
);
CREATE TABLE "maat" (
`id`    INT,
`maat`  TEXT NOT NULL,
`voorraad`      INT NOT NULL,
FOREIGN KEY(`id`) REFERENCES `items` ON UPDATE restrict ON DELETE cascade
);
CREATE TABLE `korting` (
`id`    INTEGER UNIQUE,
`korting`       INTEGER
);
CREATE TABLE "items" (
`id`    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
`naam`  TEXT NOT NULL,
`prijst`        FLOAT NOT NULL,
`categorienr`   INTEGER NOT NULL,
`beschrijving`  TEXT NOT NULL
);
CREATE TABLE gebruikers(
account_naam TEXT PRIMARY KEY NOT NULL,
password TEXT NOT NULL,
pi_code INT REFERENCES persoonlijke_informatie ON UPDATE restrict ON DELETE cascade,
permissies TEXT NOT NULL,
email TEXT NOT NULL
);
CREATE TABLE fotos(
id INT REFERENCES items ON UPDATE restrict ON DELETE cascade,
type INT NOT NULL,
url TEXT NOT NULL
);
CREATE TABLE "categorie" (
`categorienr`   INTEGER PRIMARY KEY AUTOINCREMENT,
`categorie`     INTEGER,
`doelgroep`     INTEGER
);
CREATE TABLE "blog" (
`titel` TEXT NOT NULL,
`tekst` TEXT NOT NULL,
`foto`  TEXT NOT NULL,
`url`   TEXT NOT NULL,
`id`    INTEGER,
PRIMARY KEY(`id`)
);
COMMIT;
INSERT INTO persoonlijke_informatie VALUES(0, "Foo", "9321KP", "123456789");
