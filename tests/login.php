<?php
/*
   This file is meant for testing the login, logout and register
   functions. It will create a dummy webshop, create a few users,
   tests a few conditions and look whether they are what we expected.
   After doing all of this it will do some cleanup.
 */

// Import the PHP functions
include("utils.php");
include("../php/webshop.php");
include("../php/user.php");

// Create a clean database
init_db();

// Create and login correctly.
$user = new Guest(new Webshop("fake_shop.db"));
$user->register("valentijn", "helloworld", "valentijn@linux.com");
echo "User is guest: " . check(get_class($user) == "Guest");
echo "Correct user information: " . check($user->login("valentijn", "helloworld") == true);
echo "Incorrect password: " . check($user->login("valentijn", "foobar") == false);
echo "User is still guest: " . check(get_class($user) == "Guest");
echo "Incorrect username: " . check($user->login("kek", "foobar") == false);
echo "Is of correct type: " . check(get_class($user->login("valentijn", "helloworld")) == "User");

// Remove the fake database
rm_db();
?>
