<?php
function init_db()
{
    // Create a clean database for testing
    $sql = file_get_contents("../create_database.sql");
    $db = new SQLite3("fake_shop.db");
    $db->exec($sql);
    $db->close();


    if (! file_exists("fake_shop.db"))
        exit("Failed creating the fake webshop.");
    else
        return $db;
}

function rm_db()
{
    // Remove the fake database.
    unlink("fake_shop.db");
}

function check($result)
{
    if ($result)
        return "<font style='color: green; font-style: italic;'>Correct.</font><br>";
    else
        return "<font style='color: red; font-style: italic;'>Incorrect.</font><br>";
}
?>
