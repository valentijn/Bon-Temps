function switch_div() {
    $('#login-modal').modal('toggle');
    $('#register-modal').modal('toggle');
    $('#alert').remove();
}

function verify_password() {
    var password = document.getElementById("password-reg");
    var password_verify = document.getElementById("password-verify");

    if (password.value === password_verify.value && password_verify.value.length >= 5) {
        $("#icon-verify").removeClass("glyphicon-remove");
        $("#icon-verify").addClass("glyphicon-ok");
    } else {
        $("#icon-verify").removeClass("glyphicon-ok");
        $("#icon-verify").addClass("glyphicon-remove");
    }

    if (password.value.length >= 5) {
        $("#icon-pass").removeClass("glyphicon-remove");
        $("#icon-pass").addClass("glyphicon-ok");
    } else {
        $("#icon-pass").removeClass("glyphicon-ok");
        $("#icon-pass").addClass("glyphicon-remove");
    }
}

function create_images() {
    function format (json) {
        for (var i = 0; json.length; i++) {
            var img = document.createElement("img");
            var h4 = document.createElement("h4");
            img.src = json[i]['foto'];
            h4.text = json[i]['titel'];

            document.getElementById("main").appendChild(img);
            document.getElementById("main").appendChild(h4);
        }
    }

    $.getJSON("php/api.php", { getFromDatabase: 1, row: "titel, foto", table: "blog" }, format);
}

function add_picture() {
    var selected = $("#titles :selected")[0];
    var img = document.createElement("img");
    var h4 = document.createElement("h4");

    img.src = selected.value
    h4.text = selected.label
    document.getElementById("main").appendChild(img);
    document.getElementById("main").appendChild(h4);
}

function create_dropdown() {
    $.getJSON("php/api.php", { getFromDatabase: 1, row: "titel, foto", table: "blog" },
              function (json) {
                  console.log();
                  for (var i = 0; i < json.length; i++) {
                      console.log(json[i]);
                      var option = document.createElement("option");
                      option.value = json[i]['foto'];
                      option.text  = json[i]['titel'];

                      $("#titles").append(option);
                  }
              }
             );
}

function login() {
    function handler(json) {
        console.log(json);
        if (json['error_code'] == 0) {
            $("#alert").remove();
            $("#login-modal").modal("hide");
        } else {
            $("#alert").remove();
            $("#login-body").prepend("<div class='alert alert-danger' role='alert' " +
                                     "id='alert'>" +
                                     "<span class='glyphicon glyphicon-warning-sign'></span>"
                                     + "  " + json['response'] + "</div>")
        }
    }

    $.post("php/login.php",
           {login:1, username: $("#username-log").val(), password: $("#password-log").val()},
           handler,
           "json");
}

function register() {
    function handler(json) {
        console.log(json);
        if (json['error_code'] == 0) {
            $("#alert").remove();
            $("#register-modal").modal("hide");
        } else {
            $("#alert").remove();
            $("#register-body").prepend("<div class='alert alert-danger' role='alert' " +
                                        "id='alert'>" + json['response'] + "</div>")
        }
    }

    $.post("php/login.php",
           {register: 1, username: $("#username-reg").val(),
            password: $("#password-reg").val(), email: $("#email").val()}, handler, "json");
}

var reg_button = document.getElementById('reg-button');
reg_button.addEventListener('click', switch_div);

var log_button = document.getElementById("log-button");
log_button.addEventListener('click', switch_div);

var pic_button = document.getElementById("run-sql");
pic_button.addEventListener('click', create_dropdown);

$("#password-verify").keyup(verify_password);
$("#password-reg").keyup(verify_password);
$("#login").click(login);

$.getJSON("php/api.php", {getUsername: null, getUserClass: null},
          function (json) {
              $("#user")[0].textContent = json['username']  + " " + json["class"]
          });
